class Lecture < ActiveRecord::Base
	validates :name, :description, :start_time, :end_time, presence:true
  belongs_to :edition
  belongs_to :speaker
  belongs_to :room

  validates_datetime  :end_time, :on_or_after => :start_time

  def to_s
    name
  end

end
