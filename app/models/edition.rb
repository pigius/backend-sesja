class Edition < ActiveRecord::Base
  validates :number, presence: true, uniqueness: true
  validates :address, presence: true
  has_many :lectures
  has_many :collaborator_editions
  has_many :collaborators, through: :collaborator_editions
  accepts_nested_attributes_for :collaborator_editions, reject_if: :all_blank, allow_destroy:  true

end
