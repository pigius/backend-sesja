class CollaboratorEdition < ActiveRecord::Base
  belongs_to :collaborator
  belongs_to :edition
  belongs_to :collaborator_type

  def to_s
     name
  end 
end

