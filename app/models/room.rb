class Room < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true
  has_many :lectures


  def to_s
    name
  end
  
end
