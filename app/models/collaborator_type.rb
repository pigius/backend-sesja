class CollaboratorType < ActiveRecord::Base
	validates :name, presence: true
  has_many :collaborators


  def to_s
    name
  end

end

