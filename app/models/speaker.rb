class Speaker < ActiveRecord::Base

  
	validates :first_name, :last_name, presence: true
  has_many :lectures
  has_attached_file :image, styles: { 
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "640x380>" 
    }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

 def to_s
    "#{first_name} #{last_name}"
  end
end
