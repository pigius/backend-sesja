class Collaborator < ActiveRecord::Base
		validates :name, presence: true
    has_many :collaborator_editions
    has_many :editions, through: :collaborator_editions 
    validates :website, :url => {:allow_blank => true, :message => "Niepoprawny link"} 
    accepts_nested_attributes_for :collaborator_editions, allow_destroy: true

    def to_s
     name
    end 
end

