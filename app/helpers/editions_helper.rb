module EditionsHelper

 def show_collaborators(edition) 
  collaborators_string = ""
  edition.collaborator_editions.each do |collaborator_edition|
    collaborators_string +=  "#{collaborator_edition.collaborator.name}  (#{collaborator_edition.collaborator_type.name}) "
    
  end
  collaborators_string
 end

end
