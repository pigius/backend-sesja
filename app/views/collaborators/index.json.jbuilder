json.array!(@collaborators) do |collaborator|
  json.extract! collaborator, :id, :name, :description, :website
  json.url collaborator_url(collaborator, format: :json)
end
