json.array!(@lectures) do |lecture|
  json.extract! lecture, :id, :name, :description, :start_time, :end_time
  json.url lecture_url(lecture, format: :json)
end
