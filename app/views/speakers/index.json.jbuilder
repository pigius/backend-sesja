json.array!(@speakers) do |speaker|
  json.extract! speaker, :id, :first_name, :nickname, :last_name, :description
  json.url speaker_url(speaker, format: :json)
end
