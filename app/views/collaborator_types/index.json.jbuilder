json.array!(@collaborator_types) do |collaborator_type|
  json.extract! collaborator_type, :id, :name
  json.url collaborator_type_url(collaborator_type, format: :json)
end
