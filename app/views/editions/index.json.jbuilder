json.array!(@editions) do |edition|
  json.extract! edition, :id, :number, :address
  json.collaborators edition.collaborator_editions do |collaborator_edition|
    json.name collaborator_edition.collaborator.name
    json.type collaborator_edition.collaborator_type.name
  end
  json.url edition_url(edition, format: :json)
end
