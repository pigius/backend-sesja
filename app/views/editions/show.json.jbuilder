json.extract! @edition, :id, :number, :address, :created_at, :updated_at 
json.collaborators @edition.collaborator_editions do |collaborator_edition|
    json.name collaborator_edition.collaborator.name
    json.type collaborator_edition.collaborator_type.name
  end
