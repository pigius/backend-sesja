class CollaboratorTypesController < ApplicationController
  before_action :set_collaborator_type, only: [:show, :edit, :update, :destroy]

  # GET /collaborator_types
  # GET /collaborator_types.json
  def index
    @collaborator_types = CollaboratorType.all
  end

  # GET /collaborator_types/1
  # GET /collaborator_types/1.json
  def show
  end

  # GET /collaborator_types/new
  def new
    @collaborator_type = CollaboratorType.new
  end

  # GET /collaborator_types/1/edit
  def edit
  end

  # POST /collaborator_types
  # POST /collaborator_types.json
  def create
    @collaborator_type = CollaboratorType.new(collaborator_type_params)

    respond_to do |format|
      if @collaborator_type.save
        format.html { redirect_to @collaborator_type, notice: 'Collaborator type was successfully created.' }
        format.json { render :show, status: :created, location: @collaborator_type }
      else
        format.html { render :new }
        format.json { render json: @collaborator_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /collaborator_types/1
  # PATCH/PUT /collaborator_types/1.json
  def update
    respond_to do |format|
      if @collaborator_type.update(collaborator_type_params)
        format.html { redirect_to @collaborator_type, notice: 'Collaborator type was successfully updated.' }
        format.json { render :show, status: :ok, location: @collaborator_type }
      else
        format.html { render :edit }
        format.json { render json: @collaborator_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collaborator_types/1
  # DELETE /collaborator_types/1.json
  def destroy
    @collaborator_type.destroy
    respond_to do |format|
      format.html { redirect_to collaborator_types_url, notice: 'Collaborator type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collaborator_type
      @collaborator_type = CollaboratorType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collaborator_type_params
      params.require(:collaborator_type).permit(:name)
    end
end
