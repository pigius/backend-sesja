Rails.application.routes.draw do

  apipie
	root 'dashboard#index'
  resources :editions do
    resources :lectures
    resources :rooms
  end
  resources :speakers
  resources :collaborators
  resources :collaborator_types
  delete '/speakers/remove_image/:id' => 'speakers#remove_image', as: :remove_image

end
