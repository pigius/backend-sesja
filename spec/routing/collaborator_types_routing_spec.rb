require "rails_helper"

RSpec.describe CollaboratorTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/collaborator_types").to route_to("collaborator_types#index")
    end

    it "routes to #new" do
      expect(:get => "/collaborator_types/new").to route_to("collaborator_types#new")
    end

    it "routes to #show" do
      expect(:get => "/collaborator_types/1").to route_to("collaborator_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/collaborator_types/1/edit").to route_to("collaborator_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/collaborator_types").to route_to("collaborator_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/collaborator_types/1").to route_to("collaborator_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/collaborator_types/1").to route_to("collaborator_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/collaborator_types/1").to route_to("collaborator_types#destroy", :id => "1")
    end

  end
end
