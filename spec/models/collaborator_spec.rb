require 'rails_helper'

RSpec.describe Collaborator, type: :model do
	it "can be created" do
     collaborator = FactoryGirl.create(:collaborator)
  	 expect(collaborator).not_to be_nil
    end

  it "it belongs to collaborator_type" do
     collaborator = FactoryGirl.create(:collaborator)
     expect(collaborator.collaborator_type).not_to be_nil
    end      	
end
