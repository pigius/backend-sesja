require 'rails_helper'

RSpec.describe CollaboratorType, type: :model do
  it "can be created" do
  	collaborator_type = FactoryGirl.create(:collaborator_type)
  	expect(collaborator_type).not_to be_nil
  end
  it "has Collaborator" do
    collaborator_type = FactoryGirl.create(:collaborator_type)
    expect(collaborator_type.collaborators).to be

  end
end
