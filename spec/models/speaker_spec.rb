require 'rails_helper'

RSpec.describe Speaker, type: :model do
  it "can be created" do
  	speaker = FactoryGirl.create(:speaker)
  	expect(speaker).not_to be_nil
  end 

  it "has lecture" do
    speaker = FactoryGirl.create(:speaker)
    expect(speaker.lectures).to be

  end
end
