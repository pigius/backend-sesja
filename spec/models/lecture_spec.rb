require 'rails_helper'

RSpec.describe Lecture, type: :model do
	it "can be created" do
  	lecture = FactoryGirl.create(:lecture)
  	expect(lecture).not_to be_nil

    end  	

  it "belongs to edition" do
    lecture = FactoryGirl.create(:lecture)
    expect(lecture.edition).not_to be_nil
  end

  it "belongs to room" do
    lecture = FactoryGirl.create(:lecture)
    expect(lecture.room).not_to be_nil
  end

  it "belongs to speaker" do
    lecture = FactoryGirl.create(:lecture)
    expect(lecture.speaker).not_to be_nil
  end

end
