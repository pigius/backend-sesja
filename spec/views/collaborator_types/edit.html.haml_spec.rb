require 'rails_helper'

RSpec.describe "collaborator_types/edit", type: :view do
  before(:each) do
    @collaborator_type = assign(:collaborator_type, CollaboratorType.create(
    	:name => "Współpracownik"
    	))
  end

  it "renders the edit collaborator_type form" do
    render

    assert_select "form[action=?][method=?]", collaborator_type_path(@collaborator_type), "post" do
    end
  end
end
