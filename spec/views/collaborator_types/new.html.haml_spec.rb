require 'rails_helper'

RSpec.describe "collaborator_types/new", type: :view do
  before(:each) do
    assign(:collaborator_type, CollaboratorType.new(
    	:name => "Współpracownik"))
  end

  it "renders new collaborator_type form" do
    render

    assert_select "form[action=?][method=?]", collaborator_types_path, "post" do
    end
  end
end
