require 'rails_helper'

RSpec.describe "lectures/index", type: :view do
  before(:each) do
    assign(:lectures, [
      Lecture.create!(
      	:name => "Prelegent1",
        :description => "Blabla1",
        :start_time => "10:00",
        :end_time => "12:00"
      ),
      Lecture.create!(
        :name => "Prelegent2",
        :description => "Blabla2",
        :start_time => "12:00",
        :end_time => "14:00"
      )
    ])
  end

  it "renders a list of lectures" do
    render
  end
end
