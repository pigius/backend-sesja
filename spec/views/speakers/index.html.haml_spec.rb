require 'rails_helper'

RSpec.describe "speakers/index", type: :view do
  before(:each) do
    assign(:speakers, [
      Speaker.create!(
    	:first_name => "Sebastian",
    	:nickname => "Seba",
    	:last_name => "Rakiewicz",
    	:description => "Typowy Seba"),
      Speaker.create!(
      	:first_name => "Sebastian2",
    	:nickname => "Seba2",
    	:last_name => "Rakiewicz2",
    	:description => "Typowy Seba2"
      )
    ])
  end

  it "renders a list of speakers" do
    render
  end
end
