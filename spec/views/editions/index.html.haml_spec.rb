require 'rails_helper'

RSpec.describe "editions/index", type: :view do
  before(:each) do
    assign(:editions, [
      Edition.create!(
        :number => 1,
        :address => "Address"
      ),
      Edition.create!(
        :number => 2,
        :address => "Address"
      )
    ])
  end

  it "renders a list of editions" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 1
    assert_select "tr>td", :text => 2.to_s, :count => 1
    assert_select "tr>td", :text => "Address".to_s, :count => 2
  end
end
