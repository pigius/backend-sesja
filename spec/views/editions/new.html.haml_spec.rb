require 'rails_helper'

RSpec.describe "editions/new", type: :view do
  before(:each) do
    assign(:edition, Edition.new(
      :number => 1,
      :address => "MyString"
    ))
  end

  it "renders new edition form" do
    render

    assert_select "form[action=?][method=?]", editions_path, "post" do

      assert_select "input#edition_number[name=?]", "edition[number]"

      assert_select "input#edition_address[name=?]", "edition[address]"
    end
  end
end
