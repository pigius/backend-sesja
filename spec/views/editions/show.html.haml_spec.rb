require 'rails_helper'

RSpec.describe "editions/show", type: :view do
  before(:each) do
    @edition = assign(:edition, Edition.create!(
      :number => 1,
      :address => "Address"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Address/)
  end
end
