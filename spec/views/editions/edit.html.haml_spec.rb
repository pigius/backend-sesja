require 'rails_helper'

RSpec.describe "editions/edit", type: :view do
  before(:each) do
    @edition = assign(:edition, Edition.create!(
      :number => 1,
      :address => "MyString"
    ))
  end

  it "renders the edit edition form" do
    render

    assert_select "form[action=?][method=?]", edition_path(@edition), "post" do

      assert_select "input#edition_number[name=?]", "edition[number]"

      assert_select "input#edition_address[name=?]", "edition[address]"
    end
  end
end
