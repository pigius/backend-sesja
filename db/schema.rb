# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160316192125) do

  create_table "collaborator_editions", force: :cascade do |t|
    t.integer  "collaborator_id"
    t.integer  "edition_id"
    t.integer  "collaborator_type_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "collaborator_editions", ["collaborator_id"], name: "index_collaborator_editions_on_collaborator_id"
  add_index "collaborator_editions", ["edition_id"], name: "index_collaborator_editions_on_edition_id"

  create_table "collaborator_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "collaborators", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "website"
    t.integer  "edition_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "collaborators", ["edition_id"], name: "index_collaborators_on_edition_id"

  create_table "editions", force: :cascade do |t|
    t.integer  "number"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lectures", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "edition_id"
    t.integer  "speaker_id"
    t.integer  "room_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "lectures", ["edition_id"], name: "index_lectures_on_edition_id"
  add_index "lectures", ["room_id"], name: "index_lectures_on_room_id"
  add_index "lectures", ["speaker_id"], name: "index_lectures_on_speaker_id"

  create_table "rooms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "speakers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "nickname"
    t.string   "last_name"
    t.string   "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

end
