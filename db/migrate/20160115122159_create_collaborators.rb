class CreateCollaborators < ActiveRecord::Migration
  def change
    create_table :collaborators do |t|
      t.string :name
      t.string :description
      t.string :website
      t.belongs_to :edition, index: true
      t.timestamps null: false
    end
  end
end
