class CreateLectures < ActiveRecord::Migration
  def change
    create_table :lectures do |t|
      t.string :name
      t.string :description
      t.datetime :start_time
      t.datetime :end_time
      t.belongs_to :edition, index: true
      t.belongs_to :speaker, index: true
      t.belongs_to :room, index: true

      t.timestamps null: false
    end
  end
end
