class AddAttachmentImageToSpeakers < ActiveRecord::Migration
  def self.up
    change_table :speakers do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :speakers, :image
  end
end
