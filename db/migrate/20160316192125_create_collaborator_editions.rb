class CreateCollaboratorEditions < ActiveRecord::Migration
  def change
    create_table :collaborator_editions do |t|
      t.references :collaborator, index: true, foreign_key: true
      t.references :edition, index: true, foreign_key: true
      t.references :collaborator_type, foreign_key: true
      t.timestamps null: false
    end
  end
end
