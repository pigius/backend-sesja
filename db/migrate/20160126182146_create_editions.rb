class CreateEditions < ActiveRecord::Migration
  def change
    create_table :editions do |t|
      t.integer :number
      t.string :address

      t.timestamps null: false
    end
  end
end
